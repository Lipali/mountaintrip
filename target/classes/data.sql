INSERT INTO
    users(id, login, email, password, rank, role, start_date)
VALUES
	('1', 'user1', 'dmswroc@gmail.com','admin123','NORMAL','USER', '2015-08-04'),
    ('2', 'user2','dmswroc@gmail.com','admin123','NORMAL','USER', '2015-08-04'),
    ('3', 'user3', 'dmswroc@gmail.com','admin123','NORMAL','USER', '2015-08-04');
INSERT INTO
    photos(id, file_name, scan_of_photo)
VALUES
    ('4', 'xyx', null),
    ('5', 'xyy', null),
    ('6', 'xxx', null),
    ('7', 'xyx', null),
    ('8', 'xxx', null);
INSERT INTO
    user_photos(id, user_id)
VALUES
    ('4','1'),
    ('5','2'),
    ('6','3');
INSERT INTO
    conversations(id, members_id)
VALUES
    ('9','{1,2,3}');
INSERT INTO
    messages(id, content, scan_of_photo, creation_date, conversation_id)
VALUES
    ('10','testA', null,'2015-08-12T16:00:00','9'),
    ('11','testB', null,'2015-08-12T16:01:00','9');
INSERT INTO
    private_messages(id, author_id)
VALUES
    ('12', '1'),
    ('13', '2');
INSERT INTO
    follows(id, user_id, followed_user_id)
VALUES
    ('14', '1', '2'),
    ('15', '1', '3');
INSERT INTO
    mountain_trips(id, trip_name, content, skill, max_height, superelevation, hash_tag, creation_date, user_id)
VALUES
    ('16', 'Orla Perć', 'No spoko było.', 'HARD', '2159', '1347', '{ tatry, wysokie}', '2015-08-13T16:01:00','1'),
    ('17', 'Snieżka', 'No lajtowo było.', 'MEDIUM', '1602', '800', '{ karkonosze, wysokie}', '2015-09-13T16:01:00','1');
INSERT INTO
    mountain_trip_photos(id, mountain_trip_id)
VALUES
    ('18', '1'),
    ('19', '2');
INSERT INTO
    likes(id, author_id, mountain_trip_id)
VALUES
    ('20', '2', '1'),
    ('21', '3', '1'),
    ('22', '2', '2');
INSERT INTO
    comments(id, author_id, mountain_trip_id, content)
VALUES
    ('23', '2', '1', 'no i spoko xd'),
    ('24', '3', '1', 'było miło xd');



