package envelo.MountainTrip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MountainTripApplication {

	public static void main(String[] args) {
		SpringApplication.run(MountainTripApplication.class, args);
	}

}
