package envelo.MountainTrip.repositories;

import envelo.MountainTrip.model.entities.Follow;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FollowRepository extends JpaRepository<Follow, Long> {
}
