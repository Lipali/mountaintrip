package envelo.MountainTrip.repositories;

import envelo.MountainTrip.model.entities.photo.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
}
