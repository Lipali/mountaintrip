package envelo.MountainTrip.repositories;

import envelo.MountainTrip.model.entities.MountainTrip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MountainTripRepository extends JpaRepository<MountainTrip, Long> {
}
