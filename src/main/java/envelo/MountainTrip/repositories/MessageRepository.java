package envelo.MountainTrip.repositories;

import envelo.MountainTrip.model.entities.message.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
