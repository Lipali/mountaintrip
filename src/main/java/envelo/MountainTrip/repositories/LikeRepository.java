package envelo.MountainTrip.repositories;

import envelo.MountainTrip.model.entities.Like;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LikeRepository extends JpaRepository<Like, Long> {
}
