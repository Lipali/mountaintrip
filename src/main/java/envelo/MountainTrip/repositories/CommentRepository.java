package envelo.MountainTrip.repositories;

import envelo.MountainTrip.model.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
