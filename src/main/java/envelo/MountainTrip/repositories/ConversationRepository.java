package envelo.MountainTrip.repositories;

import envelo.MountainTrip.model.entities.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConversationRepository extends JpaRepository<Conversation, Long> {
}
