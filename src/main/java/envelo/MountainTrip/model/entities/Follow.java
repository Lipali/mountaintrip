package envelo.MountainTrip.model.entities;


import jakarta.persistence.*;
import lombok.*;


@Entity
@Table(name = "follows")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter

public class Follow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long followedUserId;
}
