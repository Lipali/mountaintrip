package envelo.MountainTrip.model.entities;

import envelo.MountainTrip.model.entities.photo.MountainTripPhoto;
import envelo.MountainTrip.model.enums.Skill;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "mountainTrips")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class MountainTrip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String tripName;
    private String content;
    private List<String> hashTag;
    @Enumerated(EnumType.STRING)
    private Skill skill;
    private int maxHeight;
    private int superelevation;
    private LocalDateTime creationDate;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User author;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "mountainTrip_id")
    private List<Like> likes;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "mountainTrip_id")
    private List<Comment> comments;
    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "mountainTrip_id")
    private List<MountainTripPhoto> photos;


}
