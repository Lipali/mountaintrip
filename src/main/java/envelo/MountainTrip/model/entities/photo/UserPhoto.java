package envelo.MountainTrip.model.entities.photo;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "user_photos")
//@AllArgsConstructor
//@NoArgsConstructor
//@SuperBuilder
@Getter
@Setter
public class UserPhoto extends Photo{
}
