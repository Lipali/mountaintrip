package envelo.MountainTrip.model.entities.photo;

import envelo.MountainTrip.model.dtos.Place;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "mountainTrip_photos")
//@AllArgsConstructor
//@NoArgsConstructor
//@SuperBuilder
@Getter
@Setter
public class MountainTripPhoto extends Photo{

    Place getPlace() {
        return null;
    }
}
