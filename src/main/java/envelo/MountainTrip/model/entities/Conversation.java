package envelo.MountainTrip.model.entities;

import envelo.MountainTrip.model.entities.message.Message;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Table(name = "conversations")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Conversation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private List<Long> membersId;
    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "conversation_id")
    private List<Message> messages;
}
