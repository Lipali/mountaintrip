package envelo.MountainTrip.model.entities;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "likes")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Like {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    Long authorId;
}
