package envelo.MountainTrip.model.enums;

public enum Skill {
    EASY,
    MEDIUM,
    HARD,
    HARDCORE
}
