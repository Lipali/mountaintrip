package envelo.MountainTrip.model.dtos.mountainTrip;

import envelo.MountainTrip.model.dtos.userDto.UserDto;
import envelo.MountainTrip.model.dtos.Weather;
import envelo.MountainTrip.model.dtos.photoDto.PhotoDto;
import envelo.MountainTrip.model.enums.Skill;
import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class MountainTripLightweightDto {

    private Long id;
    private String tripName;
    private String content;
    private List<String> hashTag;
    private Skill skill;
    private Weather weather;
    private UserDto author;
    private int NumberOfLikes;
    private PhotoDto photo;
}
