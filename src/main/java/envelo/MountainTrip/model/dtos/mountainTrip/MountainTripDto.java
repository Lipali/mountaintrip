package envelo.MountainTrip.model.dtos.mountainTrip;

import envelo.MountainTrip.model.dtos.CommentDto;
import envelo.MountainTrip.model.dtos.LikeDto;
import envelo.MountainTrip.model.dtos.userDto.UserDto;
import envelo.MountainTrip.model.dtos.Weather;
import envelo.MountainTrip.model.dtos.photoDto.PhotoDto;
import envelo.MountainTrip.model.enums.Skill;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class MountainTripDto {

    private Long id;
    private String tripName;
    private String content;
    private List<String> hashTag;
    private Skill skill;
    private int maxHeight;
    private int superelevation;
    private LocalDateTime creationDate;
    private Weather weather;
    private UserDto author;
    private List<LikeDto> likes;
    private List<CommentDto> comments;
    private List<PhotoDto> photos;

}
