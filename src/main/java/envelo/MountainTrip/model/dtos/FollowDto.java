package envelo.MountainTrip.model.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class FollowDto {

    private Long id;
    private Long followedUserId;
    private Long followingUserId;
}
