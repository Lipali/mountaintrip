package envelo.MountainTrip.model.dtos.messageDto;

import envelo.MountainTrip.model.dtos.userDto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
public class PrivateMessageDto extends MessageDto {

    private UserDto consignorUser;
}
