package envelo.MountainTrip.model.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CommentDto {

    private Long id;
    private String content;
    private Long authorId;
}
