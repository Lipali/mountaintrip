package envelo.MountainTrip.model.dtos.userDto;

import envelo.MountainTrip.model.dtos.FollowDto;
import envelo.MountainTrip.model.dtos.messageDto.MessageDto;
import envelo.MountainTrip.model.dtos.photoDto.PhotoDto;
import envelo.MountainTrip.model.enums.Rank;
import envelo.MountainTrip.model.enums.Role;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserDto {

    private Long id;
    private String mail;
    private String login;
    private String password;
    private Rank rank;
    private Role role;
    private LocalDate startDate;
    private List<FollowDto> follows;
    private List<MessageDto> messages;
    private PhotoDto userPhoto;
}
