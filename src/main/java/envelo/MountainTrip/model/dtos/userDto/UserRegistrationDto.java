package envelo.MountainTrip.model.dtos.userDto;

import lombok.*;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@Getter
@Setter
public class UserRegistrationDto {

    @NotNull
    private String mail;
    @NotNull
    private String login;
    @NotNull
    private String password;
}
