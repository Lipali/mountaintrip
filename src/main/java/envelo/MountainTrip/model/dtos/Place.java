package envelo.MountainTrip.model.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter

public class Place {

    private String latitude;
    private String altitude;

}
