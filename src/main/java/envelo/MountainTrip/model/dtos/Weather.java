package envelo.MountainTrip.model.dtos;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class Weather {

    private double temperature;
    private double wind;
    private boolean rain;
    private boolean snow;
}
