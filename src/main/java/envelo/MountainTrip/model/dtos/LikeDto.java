package envelo.MountainTrip.model.dtos;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LikeDto {

    private Long id;
    Long authorId;
}
