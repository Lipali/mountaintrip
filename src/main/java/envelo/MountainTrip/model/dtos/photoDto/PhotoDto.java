package envelo.MountainTrip.model.dtos.photoDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
public class PhotoDto {

    private Long id;
    private String fileName;
    private byte[] scanOfPhoto;
}
