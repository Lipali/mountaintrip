package envelo.MountainTrip.model.dtos.photoDto;

import envelo.MountainTrip.model.dtos.Place;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
public class MountainTripPhotoDto extends PhotoDto {

    private Place place;
}
