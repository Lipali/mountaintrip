package envelo.MountainTrip.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/mountain-trip")
public class GreetingsController {

    @GetMapping("/elo")
    public ResponseEntity<String> sayHello(){
        return ResponseEntity.ok("hello");
    }

    @GetMapping("/goodBye")
    public ResponseEntity<String> sayGoodBye(){
        return ResponseEntity.ok("good bye");
    }
}
